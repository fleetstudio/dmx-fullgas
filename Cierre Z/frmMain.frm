VERSION 5.00
Begin VB.Form frmMain 
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Fullgas"
   ClientHeight    =   1275
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   2265
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   1275
   ScaleWidth      =   2265
   StartUpPosition =   2  'CenterScreen
   Begin VB.Timer TmrRefresh 
      Interval        =   1000
      Left            =   120
      Top             =   0
   End
   Begin VB.Frame fraDoc 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Documentos"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   1455
      Left            =   5400
      TabIndex        =   3
      Top             =   2760
      Width           =   2655
      Begin VB.CommandButton Command1 
         Caption         =   "Ticket-Factura"
         Height          =   375
         Index           =   3
         Left            =   480
         TabIndex        =   4
         Top             =   480
         Width           =   1935
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00000000&
         DrawMode        =   16  'Merge Pen
         Index           =   1
         X1              =   120
         X2              =   2520
         Y1              =   3720
         Y2              =   3720
      End
      Begin VB.Line Line1 
         BorderColor     =   &H00000000&
         DrawMode        =   16  'Merge Pen
         Index           =   0
         X1              =   120
         X2              =   2520
         Y1              =   2400
         Y2              =   2400
      End
   End
   Begin VB.Frame fraJournal 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Jornada"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   975
      Left            =   240
      TabIndex        =   0
      Top             =   120
      Width           =   1815
      Begin VB.CommandButton Command1 
         Caption         =   "Cierre &Z"
         Height          =   375
         Index           =   1
         Left            =   240
         TabIndex        =   2
         Top             =   360
         Width           =   1335
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Cierre &X"
         Height          =   375
         Index           =   0
         Left            =   1800
         TabIndex        =   1
         Top             =   1440
         Width           =   1335
      End
   End
   Begin VB.PictureBox EpsonFP 
      Height          =   480
      Left            =   2040
      ScaleHeight     =   420
      ScaleWidth      =   1140
      TabIndex        =   5
      Top             =   0
      Width           =   1200
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim i As Integer
Dim sLine As String
Const sSpace1 As String = "        "
Const sCross1 As String = "-----"

Private Sub cmdConnectPrinter()
    With Me
        .EpsonFP.ClosePort
        Call FPDelay
        .EpsonFP.CommPort = 0
        .EpsonFP.BaudRate = 3
        .EpsonFP.ProtocolType = protocol_Extended
        If (.EpsonFP.OpenPort) Then
            Call FPDelay
            Me.fraDoc.Enabled = True
            Me.Enabled = True
            Me.fraJournal.Enabled = True
        Else
            MsgBox "NOK"
            
        End If
    End With
End Sub

Private Sub Command1_Click(Index As Integer)
    Dim sCmd As String
    Dim sCmdExt As String
    Dim bAnswer As Boolean
    
    bAnswer = True
    i = 1
    sLine = ""
    Screen.MousePointer = 11
    
    Select Case Index
            
        Case 1
        '---------------------------
        ' Cierre Z
        '---------------------------
            sCmd = Chr$(&H8) + Chr$(&H1)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(sCmd)
            sCmdExt = Chr$(&HC) + Chr$(&H0)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(sCmdExt)
            If bAnswer Then bAnswer = Me.EpsonFP.SendCommand
            Call FPDelay
            If Me.EpsonFP.ReturnCode <> 0 Then ShowMsg
            
       
        
       
    End Select
   
    Screen.MousePointer = 1
    
End Sub



Public Sub FPDelay()
'
' Delay Sequence
'
    Dim Start1 As Single
    Start1 = Timer                  '
    Do While Me.EpsonFP.State = EFP_S_Busy
        Do While Timer < Start1 + 0.125     '   Timer delay
            DoEvents
            If Start1 > Timer Then          '   This is to
                Exit Do                     '   compensate for the
            End If                          '   AM to PM change
        Loop
    Loop                  '
End Sub

Public Sub ShowMsg()
    MsgBox "C�digo de Retorno: " + Format(Hex(Me.EpsonFP.ReturnCode), "0000") _
                + vbCrLf + "Estado Impresora: " + Format(Hex(Me.EpsonFP.PrinterStatus), "0000") _
                + vbCrLf + "Estado Fiscal: " + Format(Hex(Me.EpsonFP.FiscalStatus), "0000"), _
                vbOKOnly + vbExclamation, "Informaci�n de respuesta"
End Sub



Private Sub TmrRefresh_Timer()
TmrRefresh.Enabled = False
Facturar
TmrRefresh.Enabled = True
End Sub

Private Sub Form_Initialize()
    If AppRunning Then
        VB.Unload Me
    End If
End Sub

Private Function AppRunning() As Boolean
    cmdConnectPrinter
    
End Function

Private Function abrirFactura(clienteid As Long) As Boolean
Dim sCmd As String
Dim sCmdExt As String
Dim bAnswer As Boolean
bAnswer = True

Dim RsCliente As RDO.rdoResultset
abrirFactura = True

Set RsCliente = Rdo_AbrirTabla("select cliente.razonsocial as nombrecomprador, " _
       & " (replace (cliente.cuit,'-','')) as cuit, " _
       & " tipoiva.tipoivaid as tipoivaid, " _
       & " cliente.direccion as direccioncliente, " _
       & " barrio.nombre as barriocliente, " _
       & " localidad.nombre  as localidadcliente " _
       & " From cliente " _
       & " inner join tipoiva on tipoiva.tipoivaid = cliente.tipoivaid " _
       & " inner join barrio on barrio.barrioid = cliente.barrioid " _
       & " inner join localidad on localidad.localidadid = barrio.localidadid " _
       & " where clienteid = " & clienteid)

            sCmd = Chr$(&HB) + Chr$(&H1)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(sCmd)
            sCmdExt = Chr$(&H0) + Chr$(&H0)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(sCmdExt)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(RsCliente("nombrecomprador"))
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(" ")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(RsCliente("direccioncliente"))
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(RsCliente("barriocliente"))
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(RsCliente("localidadcliente"))
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("T")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(RsCliente("cuit"))
            If RsCliente("tipoivaid") = 1 Then If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("F")
            If RsCliente("tipoivaid") = 2 Then If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("M")
            If RsCliente("tipoivaid") = 3 Then If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("E")
            If RsCliente("tipoivaid") = 4 Then If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("I")
            If RsCliente("tipoivaid") = 5 Then If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("N")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(" ")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("")
            If bAnswer Then bAnswer = Me.EpsonFP.SendCommand
            Call FPDelay
            If Me.EpsonFP.ReturnCode <> 0 Then
                ShowMsg
                abrirFactura = False
            End If
            
End Function

Private Function agregarItemsFactura(facturaid As Long) As Boolean


Dim sCmd As String
Dim sCmdExt As String
Dim bAnswer As Boolean
bAnswer = True

Dim RsItems As RDO.rdoResultset


agregarItemsFactura = True
                             
Set RsItems = Rdo_AbrirTabla("SELECT facturaitem.cantidad*10000 as cantidad, " _
                             & " articulo.descripcion as descripcion, " _
                             & " lpad(split_part(((facturaitem.total/facturaitem.cantidad)::varchar),'.',1),2,'0')|| substring(split_part(((facturaitem.total/facturaitem.cantidad)::varchar),'.',2),0,5) as pu, " _
                             & " (select ivaid from cliente inner join factura on factura.clienteid=cliente.clienteid where facturaid =facturaitem.facturaid ) as ivaid " _
                             & " FROM facturaitem " _
                             & " inner join articulo on articulo.articuloid = facturaitem.articuloid " _
                             & " Where facturaid = " & facturaid)
    
    
    
 Do While Not RsItems.EOF
    
    sCmd = Chr$(&HB) + Chr$(&H2)
    

    If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(sCmd)
            sCmdExt = Chr$(&H0) + Chr$(&H0)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(sCmdExt)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(RsItems("descripcion"))
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(RsItems("cantidad"))
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(RsItems("pu"))
            If RsItems("ivaid") = 1 Then If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("1050")
            If RsItems("ivaid") = 2 Then If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("2100")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("")
            If bAnswer Then bAnswer = Me.EpsonFP.SendCommand
            Call FPDelay
            If Me.EpsonFP.ReturnCode <> 0 Then
                ShowMsg
                agregarItemsFactura = False
            End If
            
    RsItems.MoveNext

    Loop
           


End Function

Private Function agregarDescuentoFactura(facturaid As Long)

Dim sCmd As String
Dim sCmdExt As String
Dim bAnswer As Boolean
bAnswer = True

 sCmd = Chr$(&HB) + Chr$(&H4)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(sCmd)
            sCmdExt = Chr$(&H0) + Chr$(&H0)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(sCmdExt)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("Descuento Amigo")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("0")
            If bAnswer Then bAnswer = Me.EpsonFP.SendCommand
            Call FPDelay
            If Me.EpsonFP.ReturnCode <> 0 Then ShowMsg
End Function


Private Function agregarPagoFactura(facturaid As Long) As Boolean


Dim sCmd As String
Dim sCmdExt As String
Dim bAnswer As Boolean
bAnswer = True

agregarPagoFactura = True
 sCmd = Chr$(&HB) + Chr$(&H5)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(sCmd)
            sCmdExt = Chr$(&H0) + Chr$(&H0)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(sCmdExt)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("EFECTIVO")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField("001")
            If bAnswer Then bAnswer = Me.EpsonFP.SendCommand
            Call FPDelay
            If Me.EpsonFP.ReturnCode <> 0 Then
                ShowMsg
                agregarPagoFactura = False
            End If
End Function



Private Function cerrarFactura(facturaid As Long) As Boolean



Dim sCmd As String
Dim sCmdExt As String
Dim bAnswer As Boolean
bAnswer = True

cerrarFactura = True

sCmd = Chr$(&HB) + Chr$(&H6)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(sCmd)
            sCmdExt = Chr$(&H0) + Chr$(&H1)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(sCmdExt)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(1)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(" ")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(2)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(" ")
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(3)
            If bAnswer Then bAnswer = Me.EpsonFP.AddDataField(" ")
            If bAnswer Then bAnswer = Me.EpsonFP.SendCommand
            Call FPDelay
            If Me.EpsonFP.ReturnCode <> 0 Then
                ShowMsg
                cerrarFactura = False
            End If
                
End Function
Private Function updateFactura(facturaid As Long)
Dim sCmd As String
Dim sCmdExt As String
Dim bAnswer As Boolean
bAnswer = True

Dim RsCliente As RDO.rdoResultset

Set RsCliente = Rdo_AbrirTabla("update factura set facturaestadoid = 3 Where facturaid = " & facturaid)
                                
                                
                                


End Function



