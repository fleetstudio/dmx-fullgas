Attribute VB_Name = "M_BaseDeDatos"
Function Rdo_AbrirConexion() As Boolean
Rdo_AbrirConexion = True
On Local Error GoTo LblError
Conexion.QueryTimeout = 120
Conexion.LoginTimeout = 0
Dim TbFcCb As RDO.rdoResultset

Conexion.Connect = "Driver={PostgreSQL ANSI};Server=192.168.1.50;Port=5432;Database=fullgas_development;Uid=postgres;Pwd=verbatim;"

Conexion.EstablishConnection rdDriverNoPrompt


Exit Function
LblError:

 MsgBox "Ha surgido un error al intentar conectarse con el server"
    Rdo_AbrirConexion = False
End Function


Function Rdo_AbrirTabla(Query As String) As rdoResultset
Dim CursorActual As Byte
On Local Error GoTo LblError
CursorActual = Screen.MousePointer
Screen.MousePointer = vbHourglass
Set Rdo_AbrirTabla = Conexion.OpenResultset(Query, rdOpenKeyset, rdConcurReadOnly)
Screen.MousePointer = CursorActual

Exit Function
LblError:
    MsgBox "Error de conexion con la base de datos"
End Function


Function Rdo_RefreshFacturas() As rdoResultset
    
   Set Rdo_RefreshFacturas = Rdo_AbrirTabla("select facturaid,clienteid from factura where facturaestadoid = 1")

End Function


