VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Object = "{B52C1CDE-38E9-11D5-98EC-00C0F01C6C81}#1.0#0"; "IFEpson.ocx"
Begin VB.Form frmSpooler 
   BorderStyle     =   1  'Fixed Single
   Caption         =   " [ SPOOLER ] - Cola de Impresion"
   ClientHeight    =   3015
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6300
   ControlBox      =   0   'False
   DrawStyle       =   1  'Dash
   FillStyle       =   2  'Horizontal Line
   FontTransparent =   0   'False
   HasDC           =   0   'False
   Icon            =   "frmReImprimir.frx":0000
   KeyPreview      =   -1  'True
   LinkMode        =   1  'Source
   LinkTopic       =   "Form3"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3015
   ScaleWidth      =   6300
   ShowInTaskbar   =   0   'False
   Begin EPSON_Impresora_Fiscal.PrinterFiscal OcxFiscal 
      Left            =   2880
      Top             =   1320
      _ExtentX        =   847
      _ExtentY        =   847
   End
   Begin VB.Timer TmrRefresh 
      Interval        =   7500
      Left            =   3990
      Top             =   2685
   End
   Begin MSFlexGridLib.MSFlexGrid Grilla 
      Height          =   2610
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6255
      _ExtentX        =   11033
      _ExtentY        =   4604
      _Version        =   393216
   End
   Begin MSComctlLib.Slider SliderTmr 
      Height          =   285
      Left            =   30
      TabIndex        =   2
      Top             =   2685
      Width           =   1590
      _ExtentX        =   2805
      _ExtentY        =   503
      _Version        =   393216
      LargeChange     =   2000
      SmallChange     =   1000
      Min             =   3000
      Max             =   60000
      SelStart        =   3000
      Value           =   3000
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Refresco:"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   225
      Left            =   1725
      TabIndex        =   4
      Top             =   2715
      Width           =   795
   End
   Begin VB.Label LblTiempo 
      AutoSize        =   -1  'True
      Caption         =   "0,3seg."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   195
      Left            =   2625
      TabIndex        =   3
      Top             =   2730
      Width           =   600
   End
   Begin VB.Label LblEstado 
      Alignment       =   2  'Center
      BorderStyle     =   1  'Fixed Single
      Caption         =   "LEYENDO"
      Height          =   255
      Left            =   4635
      TabIndex        =   1
      Top             =   2715
      Width           =   1605
   End
End
Attribute VB_Name = "frmSpooler"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Const C_LEYENDO As String = "Leyendo"
Private Const C_LISTA As String = "Lista"
Private Const C_IMPRIMIENDO As String = "Imprimiendo"
Private Sub FindSpooledFC()
Dim Tabla As rdoResultset
Estado C_LEYENDO
PrepareGrilla
Set Tabla = Conexion.OpenResultset("Select factura.facturaid as FCID, '' as FCTYpe, cliente.razonsocial as Description from factura inner join cliente on cliente.clienteid = factura.clienteid  where facturaestadoid=1")
Do While Not Tabla.EOF
Grilla.AddItem Tabla!FCID & vbTab & Tabla!FCTYpe & vbTab & Tabla!Description
Tabla.MoveNext
Loop
Tabla.Close
Set Tabla = Nothing

End Sub
Private Sub Estado(Par As String)
LblEstado.Caption = Par
DoEvents
End Sub

Private Sub Form_Load()
SliderTmr.Value = TmrRefresh.Interval
TmrRefresh_Timer
End Sub

Private Sub SliderTmr_Change()
LblTiempo.Caption = Format(SliderTmr.Value / 1000, "0.00") & " seg."
End Sub

Private Sub TmrRefresh_Timer()
TmrRefresh.Enabled = False
FindSpooledFC
PrintSpooledFC
TmrRefresh.Enabled = True
Estado C_LISTA
End Sub

Private Sub PrintSpooledFC()
Dim contador As Integer
With Grilla
    While .Rows > 1
        Estado C_IMPRIMIENDO
        DoEvents
        Dim number As Long
        number = Val(ImprimirFc(Val(.TextMatrix(1, 0)), Trim(.TextMatrix(1, 1)), OcxFiscal))
        EliminarSpool Val(.TextMatrix(1, 0)), Trim(.TextMatrix(1, 1)), number
    Wend
End With
End Sub
Private Sub EliminarSpool(fc As Long, Tipo As String, number As Long)
'Borro el row del spool
Conexion.Execute "update factura set facturaestadoid=3, numeroreal=" & number & " where facturaid=" & fc
FindSpooledFC

End Sub
Private Sub PrepareGrilla()
With Grilla
.Clear
.Cols = 3
.Rows = 0
.ColWidth(0) = 1000
.ColWidth(1) = 1000
.ColWidth(2) = 3900
.AddItem "FC" & vbTab & "TIPO" & vbTab & "DESCRIPCION"
.ColAlignment(0) = flexAlignCenterCenter
.ColAlignment(1) = flexAlignCenterCenter
.ColAlignment(2) = flexAlignCenterCenter
.Col = 0
.Row = 0
.CellFontBold = True
.Col = 1
.Row = 0
.CellFontBold = True
.Col = 2
.Row = 0
.CellFontBold = True
.Rows = 2
.FixedRows = 1
.Rows = 1
.FixedCols = 0
End With

End Sub
