Attribute VB_Name = "M_Funciones"

Function DevolverAlicuotaDgi(IdAlicuota As Long) As String
Select Case IdAlicuota
    Case 1
        DevolverAlicuotaDgi = C_IVA_FINAL
    Case 2
        DevolverAlicuotaDgi = C_IVA_INSCRIPTO
    Case 3
        DevolverAlicuotaDgi = C_IVA_NO_INSCRIPTO
    Case 4
        DevolverAlicuotaDgi = C_IVA_MONOTRIBUTO
    Case 5
        DevolverAlicuotaDgi = C_IVA_EXENTO
End Select
End Function

Public Function StrRemp(Cadena As String, SubCadena As String, xCadena As String) As String
StrRemp = Cadena
If InStr(1, UCase(Cadena), UCase(SubCadena)) <= 0 Then Exit Function
StrRemp = Left(Cadena, InStr(1, UCase(Cadena), UCase(SubCadena)) - 1)
StrRemp = StrRemp + xCadena + Right(Cadena, Len(Cadena) - Len(SubCadena) + 1 - InStr(1, UCase(Cadena), UCase(SubCadena)))
End Function


Function LlenarListbox(Lista As ListBox, Query As String) As Err_LlenarListbox
Dim Tabla As rdoResultset
Set Tabla = Rdo_AbrirTabla(Query)
If Tabla.rdoColumns.Count > 2 Then
            LlenarListbox = ParametrosInvalidos
        Else
            Do While Not Tabla.EOF
                
                Lista.AddItem Trim(Tabla(1))
                Lista.ItemData(Lista.NewIndex) = Tabla(0)
                If Lista.ListCount Mod 100 = 0 Then DoEvents
                Tabla.MoveNext
            Loop
            If Lista.ListCount > 0 Then LlenarListbox = ok Else LlenarListbox = NoRegistros
        End If
Tabla.Close
Set Tabla = Nothing

End Function
Function LlenarCombobox(Combo As ComboBox, Query As String, Optional Combo2 As ComboBox = Empty, Optional Otro As Boolean = False) As Err_LlenarListbox
Dim Tabla As rdoResultset
Set Tabla = Rdo_AbrirTabla(Query)
If Tabla.rdoColumns.Count > 2 Then
            LlenarCombobox = ParametrosInvalidos
        Else
            Do While Not Tabla.EOF
                Combo.AddItem Trim(Tabla(1))
                If Not Otro Then
                    Combo.ItemData(Combo.NewIndex) = Tabla(0)
                Else
                    Combo2.AddItem Tabla(0)
                End If
                If Combo.ListCount Mod 100 = 0 Then DoEvents
                Tabla.MoveNext
            Loop
            If Combo.ListCount > 0 Then LlenarCombobox = ok Else LlenarCombobox = NoRegistros
        End If
Tabla.Close
Set Tabla = Nothing

End Function
Function ImporteSinPesos(Par As String) As Single
If InStr(1, Par, "$") > 0 Then
    ImporteSinPesos = Val(Right(Par, Len(Par) - 1))
Else
    ImporteSinPesos = Val(Par)
End If
End Function

Function Rdo_AbrirTabla(Query As String, Optional Tipos As Rdo_TiposDeLectura = 0) As rdoResultset
Dim CursorActual As Byte
On Local Error GoTo LblError
CursorActual = Screen.MousePointer
Screen.MousePointer = vbHourglass
If Tipos = 0 Then
    Set Rdo_AbrirTabla = Conexion.OpenResultset(Query, rdOpenKeyset, rdConcurReadOnly)
Else
    Set Rdo_AbrirTabla = Conexion.OpenResultset(Query, rdOpenKeyset, rdConcurRowVer)
End If
Screen.MousePointer = CursorActual

Exit Function
LblError:
    Mensaje Err.Description, , Err.number
End Function
Function Rdo_AbrirConexion() As Boolean
Rdo_AbrirConexion = True
On Local Error GoTo LblError
Conexion.QueryTimeout = 120
Conexion.LoginTimeout = 0
'Conexion.CursorDriver = rdUseIfNeeded
'Conexion.Connect = "server=" + Server + ";" + "uid=" + Usuario + ";pwd=" + Password + ";driver=" + Driver + ";Database=" + BaseDeDatos

Conexion.Connect = "Driver={PostgreSQL ANSI};Server=localhost;Port=5432;Database=fullgas;Uid=fullgas;Pwd=fullgas;"

Conexion.EstablishConnection rdDriverNoPrompt
Exit Function
LblError:

 Mensaje "Ha surgido un error al intentar conectarse con el server", , Err.number & " -" & Err.Description
    Rdo_AbrirConexion = False
End Function
Function Mensaje(Optional Texto As String = "", Optional Titulo As String = "Error", Optional Error As String = "", Optional Definido As Men_TiposDeMensaje = Usuario, Optional Estilo As VbMsgBoxStyle) As VbMsgBoxResult
Select Case Definido
    Case Men_TiposDeMensaje.Usuario
        If Error <> "" Then
            Mensaje = MsgBox(Texto, vbCritical, Titulo)
        Else
            Mensaje = MsgBox(Texto, Estilo, Titulo)
         End If
    Case Men_TiposDeMensaje.Grabacion
        Mensaje = MsgBox("�Confirma la grabaci�n?", vbQuestion + vbYesNo, "Confirmaci�n")
    Case Men_TiposDeMensaje.Borrado
        Mensaje = MsgBox("�Confirma la baja?", vbCritical + vbYesNo, "Confirmaci�n")
End Select
End Function
Function BuscarEnComboCadena(Control As ComboBox, Cadena As String)
Dim Buscar As Long
    Buscar = SendMessageStr(Control.hWnd, CB_FINDSTRING, -1&, (Cadena))
    If Buscar <> -1 Then
        Control.ListIndex = (Buscar)
    Else
        Control.ListIndex = 0
    End If
End Function
Function BuscarEnCombo(Control As ComboBox) As String
Dim Buscar As Long, Cadena As String
    Cadena = Control.SelText
    Debug.Print Cadena
    Buscar = SendMessageStr(Control.hWnd, CB_FINDSTRING, -1&, (Cadena))
    If Buscar <> -1 Then
        Control = Control.List(Buscar)
        BuscarEnCombo = Buscar
    Else
        Control = Cadena
    End If
    Control.SelStart = 0
    Control.SelLength = Len(Cadena)
    Debug.Print Control.SelText, "Proc"
End Function

Function ImprimirFc(Numero As Long, Tipo As String, OcxFiscal As EPSON_Impresora_Fiscal.PrinterFiscal) As String

Dim TbFcCb As RDO.rdoResultset
Dim TbFcDt As RDO.rdoResultset
Dim TbCl As RDO.rdoResultset
'Dim TbDeuda As RDO.rdoResultset
'Dim TbRemito As RDO.rdoResultset
Dim Importe As Single
Dim PosX As Single
Dim FlagCortado As Boolean
Dim FlagDescuento As Boolean
Dim FlagRemito As Boolean
Dim contador As Byte
With Printer
Tipo = Trim(Tipo)
Set TbFcCb = Rdo_AbrirTabla("select cliente.razonsocial as Cliente, factura.* from factura inner join cliente on cliente.clienteid = factura.clienteid Where facturaid = " & Numero, Lectura)
'   Set TbFcDt = Rdo_AbrirTabla("Select A.*,B.* from DetalleFactura A,Unidades B ,Productos C Where A.producto=C.Codigo And C.Unidad=B.Codigo And Tipo='" & Tipo & "' and A.Codigo=" & Numero & " Order By ID", Lectura)
    Set TbFcDt = Rdo_AbrirTabla("SELECT facturaitem.cantidad as Cantidad, articulo.descripcion as Descripcion, " _
                                & " facturaitem.total as Carga, '0' as Bonificacion, facturaitem.total as PU, '$' AS simbolo, " _
                                & " case when cliente.ivaid=1 then 10.5 else 21 end as IVA " _
                                & " From facturaitem " _
                                & " inner join articulo on articulo.articuloid = facturaitem.articuloid " _
                                & " inner join factura on factura.facturaid = facturaitem.facturaid  " _
                                & " inner join cliente on cliente.clienteid = factura.clienteid " _
                                & " Where facturaitem.facturaid = " & Numero, Lectura)
'If TbFcCb!Cliente = 0 Then
'Set TbCl = Rdo_AbrirTabla(" Select * from ConsumidorFinal,Alicuotas Where Id=" & Numero & " alicuotas.Descripcion='Consumidor Final'", Lectura)
'Set TbCl = Rdo_AbrirTabla("  Select Clientes.RazonSocial as RazonSocial ," _
'                                & "Clientes.Direccion as Direccion ," _
'                                & "'' As Cuit," _
'                                & "Clientes.Localidad as Localidad," _
'                                & "'' as CodigoPostal," _
'                                & "'' as Provincia,Alicuotas.Codigo IdAlicuota," _
'                                & "Alicuotas.Descripcion as Alicuota," _
'                                & "Alicuotas.Inscripto as Inscripto," _
'                                & "Alicuotas.NoInscripto as NoInscripto,0 as Porcentaje,0 as Positivo,0 as Dias,Alicuotas.Impreso,Alicuotas.CargaEnPrecio" _
'                                & "  From ConsumidorFinal Clientes, Alicuotas Where Alicuotas.Descripcion='Consumidor Final' and Id=" & Numero, Lectura)
'Else
'Set TbCl = Rdo_AbrirTabla(" Select Clientes.RazonSocial as RazonSocial ," _
'                                & "Clientes.Direccion as Direccion ," _
'                                & "Clientes.Cuit As Cuit," _
'                                & "Localidades.Localidad as Localidad," _
'                                & "Clientes.CP as CodigoPostal," _
'                                & "Provincias.Provincia as Provincia," _
'                                & "Alicuotas.Descripcion as Alicuota," _
'                                & "Alicuotas.Inscripto as Inscripto,Alicuotas.Codigo IdAlicuota," _
'                                & "Alicuotas.NoInscripto as NoInscripto,Clientes.Porcentaje,Clientes.Positivo,Dias,Impreso,CargaEnPrecio" _
'                                & " From Clientes, " _
'                                     & "Alicuotas," _
'                                     & "Localidades," _
'                                     & "Provincias " _
'                                     & " Where Clientes.Codigo=" & TbFcCb!Cliente _
'                                     & "   And Alicuotas.Codigo=Clientes.Iva  " _
'                                     & "   And Localidades.Codigo = Clientes.localidad  " _
'                                     & "   And Provincias.Codigo=Localidades.Provincia ")
 Set TbCl = Rdo_AbrirTabla(" SELECT cliente.razonsocial as Razonsocial, " _
                            & "cliente.direccion as Direccion, " _
                            & "localidad.nombre as Localidad, " _
                            & "cliente.tipoivaid as IdAlicuota, " _
                            & "localidad.codigopostal as CodigoPostal, " _
                            & "iva.valor as Alicuota, " _
                            & "0 AS CargaEnPrecio, " _
                            & "cliente.cuit as CUIT " _
                            & " FROM factura " _
                            & " inner join cliente on cliente.clienteid = factura.clienteid " _
                            & " left join iva on iva.ivaid = cliente.ivaid " _
                            & " left join barrio on barrio.barrioid = cliente.barrioid " _
                            & " left join localidad on localidad.localidadid = barrio.localidadid " _
                            & " Where facturaid = " & Numero, Lectura)
'End If
'Set TbDeuda = Rdo_AbrirTabla("Select  * from Deudas Where Fc=" & Numero & " and tipo ='" & Tipo & "'", Lectura)
'Set TbRemito = Rdo_AbrirTabla("Select  * from Remitos Where Fc=" & Numero & " and tipo ='" & Tipo & "'", Lectura)




Dim m_lImpresion As COCXPrint
Dim m_lRemito As String
Dim m_lFlagRemito As Boolean
'If Not TbDeuda.EOF Then
'    m_lRemito = DateDiff("d", TbDeuda!Fecha_Alta, TbDeuda!Fecha_Vencimiento) & " DIAS F.F. NETO " & "REMITO:0001-" & Format(TbRemito!Codigo, "00000000")
'    m_lFlagRemito = True
'End If
On Error GoTo LblFondo
Ret:
    Set m_lImpresion = New COCXPrint
    
    With m_lImpresion
        .SetearOCX OcxFiscal
        .AbrirFc Trim(TbCl!RazonSocial) & IIf(TbFcCb("Cliente") <> 0, " (" & TbFcCb("Cliente") & ")", ""), _
                 Replace(TbCl!Cuit, "-", ""), _
                 m_lRemito, _
                 TbCl!Direccion, _
                 TbCl!Localidad, _
                 DevolverAlicuotaDgi(TbCl!IdAlicuota)

        

Do While Not TbFcDt.EOF

    .ImprimirLinea TbFcDt("Cantidad"), _
                   TbFcDt("Simbolo") & " " & TbFcDt("Descripcion") & IIf(TbFcDt("Carga") > 0, "*", "") & IIf(TbFcDt("Bonificacion") < 0, "#", ""), _
                   TbFcDt("Pu"), _
                   TbFcDt("IVA")

 TbFcDt.MoveNext

Loop
    'Retorno el numero de factura para que actualicen la base
    ImprimirFc = .CerrarFc
End With
If m_lFlagRemito Then
    ImprimirRemito Numero, Tipo
End If
Exit Function
LblFondo:


If MsgBox(Err.Description & vbCrLf & "�Desea reintentar?", vbQuestion + vbYesNo) = vbYes Then
Set m_lImpresion = Nothing
GoTo Ret
End If
Set m_lImpresion = Nothing



Exit Function
PosX = 12
 .ScaleMode = 4
 
 .Font = "Tahoma"
 .FontSize = 12
ImprimirXY Format(TbFcCb!Emision, "dd/mmm/yyyy"), 67, 0
ImprimirXY TbCl!RazonSocial, PosX, 5.7
ImprimirXY TbCl!Direccion, PosX, 6.7
ImprimirXY TbCl!Localidad & IIf(Len(TbCl!CODIGOPOSTAL) > 0, "(" & TbCl!CODIGOPOSTAL & ")", ""), PosX, 7.7
ImprimirXY TbCl!Provincia, PosX, 8.7
ImprimirXY TbCl!Alicuota, PosX, 10
ImprimirXY TbCl!Cuit, 72, 10

'If Not TbDeuda.EOF Then
' ImprimirXY DateDiff("d", TbDeuda!Fecha_Alta, TbDeuda!Fecha_Vencimiento) & " DIAS F.F.  - NETO ", PosX, 12.8
' ImprimirXY TbDeuda!Fecha_Vencimiento, 50, 12.8
'
'ImprimirXY "1233", 40, 12
'
' ImprimirXY "0000-" & Format(TbRemito!Codigo, "00000000"), 62, 12.8
' FlagRemito = True
 
'Else
FlagRemito = False
 ImprimirXY " C O N T A D O ", PosX, 12.8
'End If
If TbFcCb!Cliente <> 0 Then ImprimirXY TbFcCb!Cliente, 81, 12.8

 .FontSize = 10
 
Do While Not TbFcDt.EOF
With TbFcDt
 If !Carga > 0 Then
  ImprimirXY "*", PosX - 5, 15 + contador
  FlagCortado = True
  End If
 If !Bonificacion < 0 Then
  FlagDescuento = True
  ImprimirXY "#", PosX - 5, 15 + contador
 End If
 
 ImprimirXYnum !Cantidad, 17, 15 + contador, IIf(!Decimales, "0.00", "0")
 
 ImprimirXY !Simbolo, PosX + 7, 15 + contador
 ImprimirXY !Descripcion, PosX + 12, 15 + contador
 ImprimirXYnum !Pu * IIf(TbCl!CargaEnPrecio, (TbCl!inscripto / 100) + 1, 1), 76, 15 + contador
 ImprimirXYnum !SubTotal * IIf(TbCl!CargaEnPrecio, (TbCl!inscripto / 100) + 1, 1), 89, 15 + contador
 Importe = Importe + !SubTotal
' If Mid(CStr(TbFcDt!Producto), 1, 1) = "3" Then
'   Contador = Contador + 1
'  Set TbRubro = Rdo_AbrirTabla("Select A.Descripcion From Rubros A,Productos B Where A.Codigo=B.rubro And b.Codigo=" & TbFcDt!Producto, Lectura)
'  ImprimirXY CStr(TbRubro(0)), PosX + 12, 15 + Contador
' End If
 
 .MoveNext
End With
contador = contador + 1
Loop
If Tipo = "A" Then
 ImprimirXYnum Importe, 89, 15 + 25
 ImprimirXYnum Importe, 89, 15 + 27
 
ImprimirXYnum TbCl!inscripto, 75, 15 + 28
ImprimirXY "%", 74, 15 + 28
ImprimirXYnum Importe * TbCl!inscripto / 100, 89, 15 + 28
If TbCl!noinscripto > 0 Then
 ImprimirXYnum TbCl!noinscripto, 75, 15 + 29
 ImprimirXY "%", 74, 15 + 29
 ImprimirXYnum Importe * TbCl!noinscripto / 100, 89, 15 + 29
End If
Else
End If
If Tipo = "A" Then
Printer.FontSize = 12
ImprimirXYnum TbFcCb!Importe, 88, 15 + 31
Else
Printer.FontSize = 12
ImprimirXYnum TbFcCb!Importe, 88, 10 + 31
End If
If FlagCortado Then ImprimirXY "(*) MATERIAL CORTADO A MEDIDA", PosX + 4, 15 + 25
If FlagDescuento Then ImprimirXY "(#) DESCUENTO ESPECIAL", PosX + 4, 15 + 26
ImprimirXY Numero & "(" & Tipo & ")", 76, 2
ImprimirXYFijo TbFcCb!Observacion, PosX - 6, 42, 50, 3.5
End With
Printer.EndDoc
If FlagRemito Then ImprimirRemito Numero, Tipo

End Function

Function ImprimirXY(Texto As String, X As Single, Y As Single)
With Printer
  .CurrentX = X
 .CurrentY = Y
  Printer.Print Texto
End With
End Function
Function ImprimirXYnum(num As Single, X As Single, Y As Single, Optional St As String)
Dim Str As String
If St = "" Then
    Str = Format(num, "0.00")
    With Printer
      .CurrentY = Y
     .CurrentX = X - Len(Str)
      Printer.Print Format(num, "0.00")
    End With
Else
    
    Str = Format(num, St)
    With Printer
      .CurrentY = Y
     .CurrentX = X - Len(Str) - IIf(Len(St) = 1, 3, 0)
      Printer.Print Format(num, St)
    End With

End If
End Function
Function ImprimirXYFijo(Texto As String, X As Single, Y As Single, DespX As Single, DespY As Single)
de:
For contador = 1 To Len(Texto)
 If Mid(Texto, contador, 1) = " " Then
 
  If Printer.TextWidth(Mid(Texto, 1, contador)) > DespX Then
    
    ImprimirXY Mid(Texto, 1, anterior), X, Y
    Y = Y + 1
    Texto = Right(Texto, Len(Texto) - anterior)
    anterior = 0
    GoTo de
  End If
 anterior = contador
 End If
Next
If anterior <> 0 Then ImprimirXY Texto, X, Y
End Function
Function ActualizarPrecio()
Dim Tabla As RDO.rdoResultset
Set Tabla = Conexion.OpenResultset("Select sum(Cantidad) Stock,Producto  from DetalleFactura Group By Producto")
Do While Not Tabla.EOF
If Tabla!Stock < 0 Then
Conexion.Execute "Update Productos Set Stock=Stock+" & Abs(Tabla!Stock) & " Where Codigo=" & Tabla!Producto
Else
Conexion.Execute "Update Productos Set Stock=Stock-" & Tabla!Stock & " Where Codigo=" & Tabla!Producto
End If
Tabla.MoveNext
Loop
End Function
Function ImprimirRemito(Numero As Long, Tipo As String)
Dim TbFcCb As RDO.rdoResultset
Dim TbFcDt As RDO.rdoResultset
Dim TbCl As RDO.rdoResultset
'Dim TbDeuda As RDO.rdoResultset
'Dim TbRemito As RDO.rdoResultset
Dim Importe As Single
Dim PosX As Single
Dim FlagCortado As Boolean
Dim FlagDescuento As Boolean
Dim contador As Byte
With Printer
Tipo = Trim(Tipo)
Set TbFcCb = Rdo_AbrirTabla("select cliente.razonsocial as Cliente, factura.* from factura inner join cliente on cliente.clienteid = factura.clienteid Where facturaid = " & Numero, Lectura)
'Set TbFcDt = Rdo_AbrirTabla("Select A.*,B.* from DetalleFactura A,Unidades B ,Productos C Where A.producto=C.Codigo And C.Unidad=B.Codigo And Tipo='" & Tipo & "' and A.Codigo=" & Numero, Escritura)
Set TbCl = Rdo_AbrirTabla(" Select Clientes.RazonSocial as RazonSocial ," _
                                & "Clientes.Direccion as Direccion ," _
                                & "Clientes.Cuit As Cuit," _
                                & "Localidades.Localidad as Localidad," _
                                & "Clientes.CP as CodigoPostal," _
                                & "Provincias.Provincia as Provincia," _
                                & "Alicuotas.Descripcion as Alicuota," _
                                & "Alicuotas.Inscripto as Inscripto," _
                                & "Alicuotas.NoInscripto as NoInscripto,Clientes.Porcentaje,Clientes.Positivo,Dias,Impreso,CargaEnPrecio" _
                                & " From Clientes, " _
                                     & "Alicuotas," _
                                     & "Localidades," _
                                     & "Provincias " _
                                     & " Where Clientes.Codigo=" & TbFcCb!Cliente _
                                     & "   And Alicuotas.Codigo=Clientes.Iva  " _
                                     & "   And Localidades.Codigo = Clientes.localidad  " _
                                     & "   And Provincias.Codigo=Localidades.Provincia ", Escritura)

'Set TbDeuda = Rdo_AbrirTabla("Select  * from Deudas Where Fc=" & Numero & " and tipo ='" & Tipo & "'", Escritura)
On Error Resume Next
Set Printer = ObtenerImpresora()
On Error GoTo 0
PosX = 12
 .ScaleMode = 4
 .Font = "Tahoma"
 .FontSize = 12
ImprimirXY Format(TbFcCb!Emision, "dd/mmm/yyyy"), 67, 3
ImprimirXY TbCl!RazonSocial, PosX, 5.7 + 3
ImprimirXY TbCl!Direccion, PosX, 6.7 + 3
ImprimirXY TbCl!Localidad & "(" & TbCl!CODIGOPOSTAL & ")", PosX, 7.7 + 3
ImprimirXY TbCl!Provincia, PosX, 8.7 + 3
ImprimirXY TbCl!Alicuota, PosX, 10 + 3
ImprimirXY TbCl!Cuit, 71, 10 + 3

'If Not TbDeuda.EOF Then
' ImprimirXY DateDiff("d", TbDeuda!Fecha_Alta, TbDeuda!Fecha_Vencimiento) & " DIAS F.F.  - NETO ", PosX, 12.8 + 3
'
' ImprimirXY TbDeuda!Fecha_Vencimiento, 50, 12.8 + 3
'
'ImprimirXY "1233", 40, 12
' ImprimirXY "0002-" & Format(Numero, "00000000"), 62, 12.8 + 3
' ImprimirXY TbFcCb!Cliente, 81, 12.8 + 3
'Else
 ImprimirXY " C O N T A D O ", PosX, 12.8 + 3
'End If


 .FontSize = 10
 contador = contador + 4
Do While Not TbFcDt.EOF
With TbFcDt
 If !Carga > 0 Then
  ImprimirXY "*", PosX - 5, 15 + contador
  FlagCortado = True
  End If
 If !Bonificacion < 0 Then
  FlagDescuento = True
  ImprimirXY "#", PosX - 5, 15 + contador
 End If
 ImprimirXYnum !Cantidad, 7, 15 + contador, IIf(!Decimales, "0.00", "0")
 ImprimirXY !Simbolo, 13, 15 + contador
 ImprimirXY !Descripcion, PosX + 7, 15 + contador
 'ImprimirXYnum !Pu * IIf(TbCl!CargaEnPrecio, (TbCl!inscripto / 100) + 1, 1), 76, 15 + Contador
 'ImprimirXYnum !SubTotal * IIf(TbCl!CargaEnPrecio, (TbCl!inscripto / 100) + 1, 1), 89, 15 + Contador
 Importe = Importe + !SubTotal
' If Mid(CStr(TbFcDt!Producto), 1, 1) = "3" Then
'   Contador = Contador + 1
'  Set TbRubro = Rdo_AbrirTabla("Select A.Descripcion From Rubros A,Productos B Where A.Codigo=B.rubro And b.Codigo=" & TbFcDt!Producto, Lectura)
'  ImprimirXY CStr(TbRubro(0)), PosX + 12, 15 + Contador
' End If
 
 .MoveNext
End With
contador = contador + 1
Loop
'If Tipo = "A" Then
' ImprimirXYnum Importe, 89, 15 + 25
 'ImprimirXYnum Importe, 89, 15 + 27
 
'ImprimirXYnum TbCl!inscripto, 75, 15 + 28
'ImprimirXY "%", 74, 15 + 28
'ImprimirXYnum Importe * TbCl!inscripto / 100, 89, 15 + 28
'If TbCl!noinscripto > 0 Then
' ImprimirXYnum TbCl!noinscripto, 75, 15 + 29
'ImprimirXY "%", 74, 15 + 29
' ImprimirXYnum Importe * TbCl!noinscripto / 100, 89, 15 + 29
'End If
'Else
'End If
'ImprimirXYnum TbFcCb!Importe, 89, 15 + 31
'If FlagCortado Then ImprimirXY "(*) MATERIAL CORTADO A MEDIDA", PosX + 4, 15 + 25
'If FlagDescuento Then ImprimirXY "(#) DESCUENTO ESPECIAL", PosX + 4, 15 + 26
'ImprimirXY Numero & "(" & Tipo & ")", 76, 2
'ImprimirXYFijo TbFcCb!Observacion, PosX - 6, 42, 50, 3.5
End With
Printer.EndDoc

End Function


Function ObtenerImpresora() As Printer
Dim Tmp
For Each Tmp In Printers
    If InStr(1, CStr(Tmp.DeviceName), "CITIZEN", vbTextCompare) > 0 Then
        Set ObtenerImpresora = Tmp
        Exit Function
    End If
Next
End Function
Function LogOperacion(Operacion As String, Formulario As String)
Conexion.Execute "Insert into Operacion values('" & Operacion & "','" & Formulario & "',GetDate())"
End Function


