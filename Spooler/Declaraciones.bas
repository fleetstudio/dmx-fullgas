Attribute VB_Name = "M_Declaraciones"
Public Conexion As New rdoConnection
Public Const DINERO_A_CUENTA As String = "Dinero a cuenta"

'Lista de Contantes
'Tipos de Lectura
Enum Rdo_TiposDeLectura
     Lectura = 0
     Escritura = 1
End Enum

Enum Err_LlenarListbox
    ParametrosInvalidos = -1
    NoRegistros = 0
    ok = 1
End Enum

Enum Men_TiposDeMensaje
    Usuario = -1
    Grabacion = 0
    Borrado = 1
End Enum

Public Const C_IVA_INSCRIPTO As String * 1 = "I"
Public Const C_IVA_NO_INSCRIPTO As String * 1 = "N"
Public Const C_IVA_MONOTRIBUTO As String * 1 = "M"
Public Const C_IVA_EXENTO As String * 1 = "E"
Public Const C_IVA_FINAL As String * 1 = "F"

