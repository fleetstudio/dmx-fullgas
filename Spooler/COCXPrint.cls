VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COCXPrint"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False

Option Explicit
Private Type T_SUSTITUCION
        Que As String
        Por As String
End Type

Private m_pOcxImpresion As EPSON_Impresora_Fiscal.PrinterFiscal
Private m_pFrenteIva As String
Private m_pComprobante As String
Private m_pSustituciones() As T_SUSTITUCION

Function ImprimirLinea(ByVal pCantidad As Single, _
                       ByVal pDescripcion As String, _
                       ByVal pPu As Single, _
                       Optional ByVal pIva As Single = 10.5) As Boolean
                    
Dim Cantidad As String
Dim Pu As String
Dim Iva As String

pDescripcion = SustituirDGI(pDescripcion)

If (m_pFrenteIva = C_IVA_FINAL Or _
    m_pFrenteIva = C_IVA_MONOTRIBUTO Or _
    m_pFrenteIva = C_IVA_EXENTO) Then

    pPu = pPu + pPu * pIva / 100
    
End If

Cantidad = FormatearDGI(pCantidad, 3)
Pu = FormatearDGI(pPu)
Iva = FormatearDGI(pIva)

                      
With m_pOcxImpresion
            ImprimirLinea = .SendInvoiceItem(pDescripcion, _
                                             Cantidad, _
                                             Pu, _
                                             Iva, _
                                             "M", _
                                             "0", _
                                             "0", _
                                             "", _
                                             "", _
                                             "", _
                                             "0000", _
                                             "0")
            ImprimirLinea = True
            
End With
If Not ImprimirLinea Then Err.Raise 2
End Function
Public Function AbrirFc(m_vCliente As String, _
                         m_vCuit As String, m_vRemito As String, _
                         m_vDireccion As String, _
                         Optional m_vLocalidad As String = "", _
                         Optional m_vFrenteIva As String = "F") As Boolean
                         
                         
                         
    Select Case m_vFrenteIva
        Case C_IVA_EXENTO, C_IVA_FINAL, C_IVA_MONOTRIBUTO
            m_pComprobante = "B"
        Case Else
            m_pComprobante = "A"
    End Select
                             
    If m_vRemito = "" Then m_vRemito = "CONTADO"
                         

              AbrirFc = ImprimirHeader(m_vCliente, _
                                       "", _
                                       IIf(m_vFrenteIva = C_IVA_FINAL, "", "CUIT"), _
                                       IIf(m_vFrenteIva = C_IVA_FINAL, "", m_vCuit), _
                                        m_vDireccion, _
                                        m_vLocalidad, _
                                       "", _
                                       m_vFrenteIva, _
                                       m_pComprobante, m_vRemito)
            
             
             m_pFrenteIva = m_vFrenteIva
             If Not AbrirFc Then Err.Raise 2
            
End Function
Function CerrarFc() As String
         Dim result As Boolean
         result = m_pOcxImpresion.CloseInvoice("T", m_pComprobante, "FIN COMPROBANTE")
         If Not result Then Err.Raise 2
         CerrarFc = m_pOcxImpresion.AnswerField_3
End Function
Private Function ImprimirHeader(ByVal pEmpresa As String, _
                        ByVal pResponsable As String, _
                        ByVal pTipoDocumento As String, _
                        ByVal pCuit As String, _
                        ByVal pDireccion As String, _
                        ByVal pLocalidad As String, _
                        ByVal pProvincia As String, _
                        ByVal pFrenteIva As String, _
                        ByVal pFormulario As String, pRemito As String) As Boolean
With m_pOcxImpresion

'Parseo el texto por si las dudas
pEmpresa = SustituirDGI(pEmpresa)
pResponsable = SustituirDGI(pResponsable)
pDireccion = SustituirDGI(pDireccion)
pLocalidad = SustituirDGI(pLocalidad)
pProvincia = SustituirDGI(pProvincia)
           
           'pCuit = "30708130563"
               ImprimirHeader = .OpenInvoice("T", _
                                          "C", _
                                          pFormulario, _
                                          "1", _
                                          "P", _
                                          "12", _
                                           C_IVA_INSCRIPTO, _
                                          pFrenteIva, _
                                          pEmpresa, _
                                          pResponsable, _
                                          pTipoDocumento, _
                                          pCuit, _
                                          "N", _
                                          pDireccion, _
                                          pLocalidad, _
                                          "", _
                                          pRemito, _
                                          "", _
                                           "C")
                                          
End With

If Not ImprimirHeader Then Err.Raise 2
End Function
Private Function FormatearDGI(ByVal Valor As Single, Optional Decimales As Integer = 2) As String
FormatearDGI = Replace(Replace(Format(Valor, "0000000." & Left("0000000", Decimales)), ",", ""), ".", "")
End Function

Public Function SetearOCX(ByVal m_vOcx As EPSON_Impresora_Fiscal.PrinterFiscal)
Set m_pOcxImpresion = m_vOcx
End Function

Private Sub Class_Initialize()
'Dim Tabla As rdoResultset
'Set Tabla = Rdo_AbrirTabla("Select * from llanmetalnew..FiscalReplace", Escritura)
'ReDim Preserve m_pSustituciones(Tabla.RowCount)
'Do While Not Tabla.EOF
'
'     m_pSustituciones(Tabla.AbsolutePosition).Que = Tabla("Llave")
'     m_pSustituciones(Tabla.AbsolutePosition).Por = Tabla("Valor")
'    Tabla.MoveNext
'Loop
'Tabla.Close
'Set Tabla = Nothing
End Sub
Private Function SustituirDGI(m_vCadena As String) As String
'Dim m_lContador As Integer
'SustituirDGI = m_vCadena
'For m_lContador = 1 To UBound(m_pSustituciones)
'    SustituirDGI = Replace(SustituirDGI, _
'                           m_pSustituciones(m_lContador).Que, _
'                           m_pSustituciones(m_lContador).Por)
'Next
SustituirDGI = m_vCadena
End Function
