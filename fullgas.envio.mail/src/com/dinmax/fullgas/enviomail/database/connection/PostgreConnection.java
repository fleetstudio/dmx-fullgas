package com.dinmax.fullgas.enviomail.database.connection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import com.dinmax.fullgas.enviomail.mail.Mail;


public class PostgreConnection {
	

    private static Properties getPropertiesMail(){
		try{
			Properties info  = new Properties();
			info.load(Mail.class.getResourceAsStream( "/databaseconnection.properties" ));
			return info;
			}catch (IOException e) {
	        	 System.out.println(e);
			}
		return null;
	}

	private static String getConnectionString(){
		return getPropertiesMail().getProperty("connection");
	}

	private static String getUser(){
		return getPropertiesMail().getProperty("user");
	}

	private static String getPass(){
		return getPropertiesMail().getProperty("pass");
	}


	public static Connection getConnection(){
		String driver = "org.postgresql.Driver";
		
		try {
			try {
				Class.forName(driver);
				return  DriverManager.getConnection(getConnectionString(), getUser() ,getPass());
				} catch (ClassNotFoundException e) {
					e.printStackTrace();
					System.out.println(e.toString());
				}
		} catch (SQLException e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		return null;
	}
	
	
	public static ResultSet getStatement( String stat){
		try{
			System.out.println(stat);
			Connection con = getConnection();
			Statement stmt =con.createStatement();

			ResultSet rs = stmt.executeQuery(stat);
			
			//stmt.close();
			con.close();
			return rs;
			}

			catch ( Exception e ){
			System.out.println(e.getMessage());
			}
		return null;
			
	}
	
	
	public static void executeStatement(String query) {
		try{
			System.out.println(query);
			Connection con = getConnection();
			Statement stmt =con.createStatement();
			stmt.execute(query);
			con.commit();
			con.close();
			}

			catch ( Exception e ){
			System.out.println(e.getMessage());
			}		
	}
	
	

	public static void main(String[] args)	{		
		getStatement( "SELECT * FROM recurso");
	}

}

