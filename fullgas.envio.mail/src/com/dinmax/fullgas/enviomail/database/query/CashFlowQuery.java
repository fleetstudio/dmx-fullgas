package com.dinmax.fullgas.enviomail.database.query;

import java.sql.ResultSet;
import java.util.Calendar;

import com.dinmax.fullgas.enviomail.database.connection.PostgreConnection;

public class CashFlowQuery {

	
	public static String getCashFlowReducido(){
		   String text = "";
		   ResultSet rs = PostgreConnection.getStatement(getQueryCashFlowReducido());
		   
		   try {
			   if (!rs.next()) {
					Calendar calendario = Calendar.getInstance();
					int dia,mes,anio;
					dia =calendario.get(Calendar.DATE);
					mes=calendario.get(Calendar.MONTH)+1;
					anio=calendario.get(Calendar.YEAR);
				   text="<P STYLE=\"margin-bottom: 0cm\"><FONT SIZE=4><U><B>Resumen al dia"+"  "+dia+"/"+mes+"/"+anio+" </B></U></FONT></P>" +
				   		"<P STYLE=\"margin-bottom: 0cm; text-decoration: none\"><B>Sin Informacion </B> </P>"; 
						
						System.out.println(text );			
				}else{
					// while (){
						   text="<P STYLE=\"margin-bottom: 0cm\"><FONT SIZE=4><B>Resumen al dia"+"  "+rs.getString("fecha")+"</B></FONT></P>" +
								"<P STYLE=\"margin-bottom: 0cm; text-decoration: none\"><U>Total Kgs. del dia:</U> "+rs.getString("totalkg")+"</P>" +
								"<P STYLE=\"margin-left: 1cm; text-decoration: none\"><U>         Butano:</U> "+rs.getString("totalkgbutano")+"</P>" +
								"<P STYLE=\"margin-left: 1cm; text-decoration: none\"><U>         Propano:</U> "+rs.getString("totalkgpropano")+"</P>" +
								"<P STYLE=\"margin-bottom: 0cm; text-decoration: none\"><U>Acumulado Mes:</U> "+rs.getString("totalkgacum")+"</P>" +
								"<P STYLE=\"margin-left: 1cm; text-decoration: none\"><U>         Butano:</U> "+rs.getString("totalkgbutanoacum")+"</P>" +
								"<P STYLE=\"margin-left: 1cm; text-decoration: none\"><U>         Propano:</U> "+rs.getString("totalkgpropanoacum")+"</P>" +
							//	"<P STYLE=\"margin-bottom: 0cm; text-decoration: none\"><FONT COLOR=\"#008000\"><B>Ingreso:</B> </FONT><FONT COLOR=\"#008000\">"+rs.getString("ingreso")+"</FONT></P>" +
							//	"<P STYLE=\"margin-bottom: 0cm; text-decoration: none\"><FONT COLOR=\"#ff0000\"><B>Egreso:</B> </FONT><FONT COLOR=\"#ff0000\">"+rs.getString("egreso")+"</FONT></P>" +
								"<P STYLE=\"margin-bottom: 0cm; text-decoration: none\"><FONT COLOR=\"#008000\"><U>Recaudacion:</U> </FONT><FONT COLOR=\"#008000\">"+rs.getString("recaudacion")+"</FONT></P>" +
								"<P STYLE=\"margin-bottom: 0cm; text-decoration: none\"><FONT COLOR=\"#ff0000\"><U>Gastos:</U> </FONT><FONT COLOR=\"#ff0000\">"+rs.getString("gastos")+"</FONT></P>" +
								"<P STYLE=\"margin-bottom: 0cm; text-decoration: none\"><B>Saldo: </B>"+rs.getString("saldo")+"</P>" +
								"<P STYLE=\"margin-bottom: 0cm; text-decoration: none\"><BR></P>";
								
							System.out.println(text );			
					//		}
				}
		  
		   
		  
			return  text;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		return null;
		}
	
	
	private static String getQueryCashFlowReducido(){
		String fecharesumen="current_date";
//		String fecharesumen="current_date-4";
		return "select (select date_part('day',cashflowreducido.fecha))||'/'||(select date_part('month',cashflowreducido.fecha))||'/'||(select date_part('year',cashflowreducido.fecha)) as fecha, "+
			   "		(select to_char(ingreso, 'FM999,999,999,999D00')) as ingreso,    "+
		   	   "		(select to_char(egreso,  'FM999,999,999,999D00')) as egreso,    "+
		   	   "		(select to_char(saldo,   'FM999,999,999,999D00')) as saldo,   "+
		   	   "		(select  case when sum(cantidad * articulo.kilos) is null then '0' else to_char(sum(cantidad * articulo.kilos),   'FM999,999,999,999D00') end "+   
			   	   "		from traslado    "+
			   	   "		inner join trasladoitem on trasladoitem.trasladoid = traslado.trasladoid "+   
			   	   "		inner join articulo  on articulo.articuloid = trasladoitem.articuloid      "+
			   	   "		where fecha =cashflowreducido.fecha     "+
			   	   "		and trasladoitem.articuloestadoid=1)   as totalkg , "+
		   	   "		(select  case when sum(cantidad * articulo.kilos) is null then '0' else to_char(sum(cantidad * articulo.kilos),   'FM999,999,999,999D00') end "+   
			   	   "		from traslado    "+
			   	   "		inner join trasladoitem on trasladoitem.trasladoid = traslado.trasladoid "+   
			   	   "		inner join articulo  on articulo.articuloid = trasladoitem.articuloid "+     
			   	   "		where fecha =cashflowreducido.fecha     "+
			   	   "		and trasladoitem.articuloestadoid=1 "+
			   	   "		and articulotipoid=1) as totalkgbutano, "+
		   	   "		(select  case when sum(cantidad * articulo.kilos) is null then '0' else to_char(sum(cantidad * articulo.kilos),   'FM999,999,999,999D00') end "+   
			   	   "		from traslado    "+
			   	   "		inner join trasladoitem on trasladoitem.trasladoid = traslado.trasladoid "+   
			   	   "		inner join articulo  on articulo.articuloid = trasladoitem.articuloid      "+
			   	   "		where fecha =cashflowreducido.fecha     "+
			   	   "		and trasladoitem.articuloestadoid=1 "+
			   	   "		and articulotipoid=2) as totalkgpropano, "+  
		   	   "		 "+
		   	   "		(select  case when sum(cantidad * articulo.kilos) is null then '0' else to_char(sum(cantidad * articulo.kilos),   'FM999,999,999,999D00') end "+   
			   	   "		from traslado    "+
			   	   "		inner join trasladoitem on trasladoitem.trasladoid = traslado.trasladoid "+   
			   	   "		inner join articulo  on articulo.articuloid = trasladoitem.articuloid      "+
			   	   "		where ((date_part('month', fecha)= date_part('month', "+fecharesumen+")) and (date_part('year', fecha)= date_part('year', "+fecharesumen+"))) "+
			   	   "		and trasladoitem.articuloestadoid=1)   as totalkgacum , "+
		   	   "		(select  case when sum(cantidad * articulo.kilos) is null then '0' else to_char(sum(cantidad * articulo.kilos),   'FM999,999,999,999D00') end "+   
			   	   "		from traslado    "+
			   	   "		inner join trasladoitem on trasladoitem.trasladoid = traslado.trasladoid "+   
			   	   "		inner join articulo  on articulo.articuloid = trasladoitem.articuloid      "+
			   	   "		where ((date_part('month', fecha)= date_part('month', "+fecharesumen+")) and (date_part('year', fecha)= date_part('year', "+fecharesumen+"))) "+
			   	   "		and trasladoitem.articuloestadoid=1 "+
			   	   "		and articulotipoid=1) as totalkgbutanoacum, "+
		   	   "		(select  case when sum(cantidad * articulo.kilos) is null then '0' else to_char(sum(cantidad * articulo.kilos),   'FM999,999,999,999D00') end "+   
			   	   "		from traslado    "+
			   	   "		inner join trasladoitem on trasladoitem.trasladoid = traslado.trasladoid "+   
			   	   "		inner join articulo  on articulo.articuloid = trasladoitem.articuloid      "+
			   	   "		where ((date_part('month', fecha)= date_part('month', "+fecharesumen+")) and (date_part('year', fecha)= date_part('year', "+fecharesumen+"))) "+
			   	   "		and trasladoitem.articuloestadoid=1 "+
			   	   "		and articulotipoid=2) as totalkgpropanoacum," +
			   	   "     (select to_char((select  sum(cantidad * trasladorendicionitem.precio)" +
			   	   "        + (select sum(deudasanteriores) from traslado" +
			   	   "           where fecha = "+fecharesumen+")" +
			   	   "        + (select ( select sum(importe) as total" +
			   	   "                     from cobro where fecha = "+fecharesumen+" and cuentaid = 2)" +
			   	   "                     -" +
			   	   "                     (select  sum(cantidad * trasladorendicionitem.precio) as total" +
			   	   "                     from trasladorendicionitem" +
			   	   "                          inner join traslado on traslado.trasladoid = trasladorendicionitem.trasladoid" +
			   	   "                     where fecha = "+fecharesumen+")" +
			   	   "                     +" +
			   	   "                     (select  sum(fiado)  - sum(deudasanteriores)" +
			   	   "                     from traslado" +
			   	   "                     where fecha = "+fecharesumen+") )" +
			   	   "        - (select sum(fiado) from traslado" +
			   	   "           where fecha = "+fecharesumen+")as recaudacion" +
			   	   "			from trasladorendicionitem" +
			   	   "				inner join traslado on traslado.trasladoid = trasladorendicionitem.trasladoid" +
			   	   "			where fecha = "+fecharesumen+"),   'FM999,999,999,999D00') ) as recaudacion," +
			   	   "		(select to_char(qy_resumen_cashflow_cuenta_importe, 'FM999,999,999,999D00') " +
			   	   "		 from qy_resumen_cashflow_cuenta_importe("+fecharesumen+","+fecharesumen+",0,'',0,0)) as 	gastos																			"+
			   "		from sp_cashflowreducido(0)  as cashflowreducido  "+
		   	   "		where cashflowreducido.fecha = "+fecharesumen+"";
		
	}
	

	public static ResultSet getSenderResumenCashFlowReducido(int dayofweek){
		   ResultSet rs = PostgreConnection.getStatement(getQuerySenderResumenCashFlowReducido(dayofweek));
		   return rs;
		}
	
	
	private static String getQuerySenderResumenCashFlowReducido(int dayofweek){
		return "select mail " +
				"from recurso " +
				"     inner join envioresumencashflow on envioresumencashflow.recursoid=recurso.recursoid " +
				"where envioresumencashflow is true " +
				"       and ( " +
				"           (domingo and "+dayofweek+" =1) or " +
				"           (lunes and "+dayofweek+" =2) or " +
				"           (martes and "+dayofweek+" =3) or " +
				"           (miercoles and "+dayofweek+" =4) or " +
				"           (jueves and "+dayofweek+" =5) or " +
				"           (viernes and "+dayofweek+" =6) or " +
				"           (sabado and "+dayofweek+" =7) " +
				"           ) ";
		
	}
}
