package com.dinmax.fullgas.enviomail.database.query;

import java.sql.ResultSet;
import com.dinmax.fullgas.enviomail.database.connection.PostgreConnection;

public class MixQuery {

	

	public static String getResumenMIX(String message){
		   String text = "";
		   ResultSet rs = PostgreConnection.getStatement(getQueryResumenMIX());
		   
		   try {
		   while (rs.next()){
			   text="Resumen MIX "+ rs.getString("nombredia")+" "+rs.getString("dia")+" a las "+rs.getString("hora")+"\n"+
			   		"	Cantidad de Kilos: "+rs.getString("kilos")+"\n"+
			   		"	Cobrado: "+rs.getString("precio")+"\n"+
			   		"	MIX: "+rs.getString("mix")+"\n";
				System.out.println(text );			
				}
		  
			return  message+text;
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println(e.toString());
		}
		return null;
	   }
	
	
	private static String getQueryResumenMIX(){
		return "select dia,nombredia,kilos,precio,mix::float," +
			      "		(select date_part('hour',current_time))||':'||(select date_part('minute',current_time)) as hora" +
				  "	 from sp_calcular_mix_diario((select date_part('month',current_date)::integer)," +
				  "  							(select date_part('year',current_date)::integer)" +
				  "   							)" +
				  "  where dia = (select date_part('day',current_date))";
	}
	
	   
}
