package com.dinmax.fullgas.enviomail.database.query;

import java.sql.ResultSet;

import com.dinmax.fullgas.enviomail.database.connection.PostgreConnection;
import com.dinmax.fullgas.enviomail.mail.Mail;

public class AlertaGastoQuery {

	
	public static String getAlertaGasto(int dayofweek){
		   String text = "";
		   ResultSet rs = PostgreConnection.getStatement(getQueryAlertaGasto());
		   
		   try {
			   while (rs.next()) {
				   text="<P STYLE=\"margin-bottom: 0cm\"><FONT SIZE=4><B>"+"  "+rs.getString("cuerpo")+"</B></FONT></P>";
					System.out.println(text );	
					ResultSet rsEmails = PostgreConnection.getStatement(getQuerySenderAlertaGasto(dayofweek));
					if (Mail.sendMailAlertaGasto(text,rsEmails)) {
						String to = "";
						rsEmails = PostgreConnection.getStatement(getQuerySenderAlertaGasto(dayofweek));
						while (rsEmails.next()) {
				               to=to+rsEmails.getString("mail")+";";
				        }
						updateMailAlertaGasto(" update email set enviado=true, " +
															"	fechaenviado=current_timestamp, " +
															" 	destinatario='"+to+"'"+
											  "	where emailid="+rs.getString("emailid"));	
					}					
				} 		  
			   return  text;
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println(e.toString());
			}
	return null;
	}	
	
	private static void updateMailAlertaGasto(String string) {
		PostgreConnection.executeStatement(string);
		
	}

	private static String getQueryAlertaGasto(){
		//return "select * from email where asunto ilike 'Alerta Ingreso de Gasto'  and enviado is false;";
		return "select * from email where asunto ilike 'Alerta Ingreso de Gasto'  and not enviado order by emailid;";
		
	}
	
	public static ResultSet getSenderAlertaGasto(int dayofweek){
		   ResultSet rs = PostgreConnection.getStatement(getQuerySenderAlertaGasto(dayofweek));
		   return rs;
	}
		
	private static String getQuerySenderAlertaGasto(int dayofweek){
		return "select mail " +
				"from recurso " +
				"     inner join envioalertagasto on envioalertagasto.recursoid=recurso.recursoid " +
				"where envioalertagasto is true " +
				"      and ( " +
				"           (domingo and "+dayofweek+" =1) or " +
				"           (lunes and "+dayofweek+" =2) or " +
				"           (martes and "+dayofweek+" =3) or " +
				"           (miercoles and "+dayofweek+" =4) or " +
				"           (jueves and "+dayofweek+" =5) or " +
				"           (viernes and "+dayofweek+" =6) or " +
				"           (sabado and "+dayofweek+" =7) " +
				"           )";		
	}

}
