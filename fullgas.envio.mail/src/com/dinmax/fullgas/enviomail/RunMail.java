package com.dinmax.fullgas.enviomail;

import java.io.IOException;
import java.util.Calendar;
import java.util.Properties;

import com.dinmax.fullgas.enviomail.mail.Mail;


public class RunMail extends Thread {
	
    public RunMail() {
        
    }
    
    private static Properties getPropertiesMail(){
		try{
			Properties info  = new Properties();
			info.load(Mail.class.getResourceAsStream( "/runconfig.properties" ));
			return info;
			}catch (IOException e) {
	        	 System.out.println(e);
			}
		return null;
	}

	private static String getHora(){
		return getPropertiesMail().getProperty("hora");
	}

	private static String getMinutos(){
		return getPropertiesMail().getProperty("minutos");
	}

	private static String getSleep(){
		return getPropertiesMail().getProperty("sleep");
	}
	
	
	
    public void run() {
		super.run();
		while (true) {
			Calendar calendario = Calendar.getInstance();
			int hora, minutos, segundos,dayofweek;
			hora =calendario.get(Calendar.HOUR_OF_DAY);
			minutos = calendario.get(Calendar.MINUTE);
			segundos = calendario.get(Calendar.SECOND);
			dayofweek= calendario.get(Calendar.DAY_OF_WEEK);
			
			System.out.println(hora + ":" + minutos + ":" + segundos);
			if (hora==(new Long(getHora())) && minutos==(new Long(getMinutos())) ) {
				Mail.sendMail(dayofweek);			
			}
			Mail.getAlertaGasto(dayofweek);
			
			try {
				sleep(new Long(getSleep()));
			} catch (InterruptedException e) {
				e.printStackTrace();
				System.out.println(e.toString());
			}
		}	
    }
    
    public static void main (String [] args) {
        new RunMail().start();
    }
}