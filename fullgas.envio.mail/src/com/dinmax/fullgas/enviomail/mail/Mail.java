package com.dinmax.fullgas.enviomail.mail;

import java.io.IOException;
import java.sql.ResultSet;
import java.util.Properties;

import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import com.dinmax.fullgas.enviomail.database.query.AlertaGastoQuery;
import com.dinmax.fullgas.enviomail.database.query.CashFlowQuery;



public class Mail {
	
   
	private static Properties getPropertiesMail(){
		try{
			Properties info  = new Properties();
			info.load(Mail.class.getResourceAsStream( "/mail.properties" ));
			return info;
			}catch (IOException e) {
	        	 System.out.println(e);
			}
		return null;
	}

	private static String getMailSend(){
		return getPropertiesMail().getProperty("mailsend");
	}

	private static String getPass(){
		return getPropertiesMail().getProperty("pass");
	}

	private static String getImageHeaderFile(){
		return getPropertiesMail().getProperty("imageheaderfile");
	}
	
	

 	private static Session getSession(){
 	   // Propiedades de la conexión
        Properties props = new Properties();
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.starttls.enable", "true");
        props.setProperty("mail.smtp.port", "587");
        props.setProperty("mail.smtp.auth", "true");

		return Session.getDefaultInstance(props);		
	}
	
	
   public static void sendMail(int dayofweek){
	   try
       {
		   Session session=getSession();
		   
           // Construir el mensaje
           MimeMessage message = new MimeMessage(session);
           /*
   		   private static String message="";
   		   private static Object body;
           message.addRecipient( Message.RecipientType.TO,new InternetAddress("sebastian.guerriere@fullgas.com.ar"));
           message.addRecipient( Message.RecipientType.TO,new InternetAddress("edgardo.guerriere@fullgas.com.ar"));
           message.addRecipient( Message.RecipientType.TO,new InternetAddress("leonardo.querol@fullgas.com.ar"));
           */
           ResultSet rs = CashFlowQuery.getSenderResumenCashFlowReducido(dayofweek);
           
           while (rs.next()) {
               message.addRecipients(Message.RecipientType.TO,rs.getString("mail"));
           }
           
    //     message.addRecipient( Message.RecipientType.BCC,new InternetAddress("emiliano.cesarano@dinmax.com"));
                                            
           message.setSubject("Fullgas Resumen Diario"); 
		   
		   message.setContent(getMultipart(getBody()));				
				
	//	   message.setContent( getBody(), "text/html");
           		   
           // Enviar
           Transport t = session.getTransport("smtp");
           t.connect(getMailSend(), getPass());
           t.sendMessage(message, message.getAllRecipients());

           // Cierre.
           t.close();
       }
       catch (Exception e)
       {
           e.printStackTrace();
		   System.out.println(e.toString());
       }
   }
      
   private static String getBody() {		   
	return 	CashFlowQuery.getCashFlowReducido();
   }

	public static void getAlertaGasto(int dayofweek) {
		AlertaGastoQuery.getAlertaGasto(dayofweek);
	}

	public static boolean  sendMailAlertaGasto(String bodyText, ResultSet rsMail) {
		   try
	       {
			   Session session=getSession();
			   
	           // Construir el mensaje
	           MimeMessage message = new MimeMessage(session);
	           /*
	           message.addRecipient( Message.RecipientType.TO,new InternetAddress("sebastian.guerriere@fullgas.com.ar"));
	           */	           
	           while (rsMail.next()) {
	               message.addRecipients(Message.RecipientType.TO,rsMail.getString("mail"));
//		           message.addRecipient( Message.RecipientType.BCC,new InternetAddress("emiliano.cesarano@dinmax.com"));
                   
		           message.setSubject("Alerta Ingreso de Gasto"); 
		          			   
				   message.setContent(getMultipart(bodyText));												
				   //message.setContent( getBody(), "text/html");
		           		   
		           // Enviar
		           Transport t = session.getTransport("smtp");
		           t.connect(getMailSend(), getPass());
		           t.sendMessage(message, message.getAllRecipients());

		           // Cierre.
		           t.close();
		           return true;
	           }
	           
	       }
	       catch (Exception e)
	       {
	           e.printStackTrace();
			   System.out.println(e.toString());
	           return false;
	       }
		return false;	
		
	}

	public static Multipart getMultipart(String bodyText){
		try
	       {
		 Multipart multipart= new MimeMultipart("related");
         BodyPart text = new MimeBodyPart();
		   String body = "<img src=\"cid:header\">"+
				  "<br>"+
				  bodyText+	
				  "<br>"
				  //+"<img src=\"cid:footer\">"
				  ;
		   text.setContent(body, "text/html");
		   multipart.addBodyPart(text);
			
		   MimeBodyPart imagenHeader = new MimeBodyPart();
		   //imagenHeader.attachFile("fullgas-header.jpg");
		   imagenHeader.attachFile(getImageHeaderFile());
		   //System.out.println("Directorio ejecucion = " + System.getProperty("user.dir"));
		   imagenHeader.setHeader("Content-ID", "<header>");
		   multipart.addBodyPart(imagenHeader);
			
		   /*MimeBodyPart imagenFooter = new MimeBodyPart();
		   imagenFooter.attachFile("./img/footer.jpg");
		   imagenFooter.setHeader("Content-ID", "<footer>");
		   multipart.addBodyPart(imagenFooter);*/
		   
		   return multipart;
	       }
	       catch (Exception e)
	       {
	           e.printStackTrace();
			   System.out.println(e.toString());
	       }
		return null;
		
	}
	
}